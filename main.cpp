#define _CRT_SECURE_NO_WARNINGS
#pragma comment (lib, "msmpi.lib")

#include <mpi.h>
#include <omp.h>
#include <iostream>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <list>
#include <cmath>
using namespace cv;

bool validate(Point* points, Point p, int n)
{
    bool inside = false;
    for (int i = 0, j = n - 1; i < n; j = i++)
    {
        int xi = points[i].x, yi = points[i].y;
        int xj = points[j].x, yj = points[j].y;

        int intersect = ((yi > p.y) != (yj > p.y))
            && (p.x < (xj - xi)* (p.y - yi) / (yj - yi) + xi);
        if (intersect) inside = !inside;
    }
    return inside;
}

void drawFun(int* imgPartCoords, Point* points, const int numOfPoints, 
             const int xMin, const int xMax, const int yMin, const int yMax, const int rank)
{
    int index = 0;
    for (int i = yMin; i < yMax + 1; ++i)
    {
        for (int j = xMin; j < xMax + 1; ++j)
        {
            if (validate(points, Point(j, i), numOfPoints))
            {
                imgPartCoords[index++] = j;
                imgPartCoords[index++] = i;
            }
        }
    }
}

int main(int argc, char** argv)
{
    int mpiRank, mpiSize;
    MPI_Status mpiStatus;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &mpiRank);
    MPI_Comm_size(MPI_COMM_WORLD, &mpiSize);

    const int numOfPoints = 8;
    const int imgSize = 600;
    const int imgSizePerProc = imgSize / mpiSize;

    const int maxBufferSize = imgSizePerProc * imgSize * 2;
    //Coordinates of points to fill with color
    //it goes like: x1, y1, x2, y2....
    //-1 means uinitialized data
    //receiver (rank == 0) stores here data from one sender process at the time
    //sender (rank > 0) stores here calculated data and sends it to receiver
    int* imgPartCoords = new int[maxBufferSize] {};

#pragma region receiver
    if (mpiRank == 0)
    {
        const Scalar green = { 0, 255, 0 };
        Mat image(imgSize, imgSize, CV_8UC3, Scalar(255, 255, 255));
        if (!image.data)
        {
            std::cout << "Could not open or find the image\n";
            return 0;
        }

        std::cout << "Receiver waiting for messages..." << std::endl;
        double start = MPI_Wtime();

        // receive partial data from every other process
        for (int currentRank = 1; currentRank < mpiSize; currentRank++)
        {
            MPI_Recv(imgPartCoords, maxBufferSize, MPI_INT, currentRank, 0, MPI_COMM_WORLD, &mpiStatus);
            int tmpX = 0, tmpY = 0, i = 0;
            do
            {
                tmpX = imgPartCoords[i++];
                tmpY = imgPartCoords[i++];
                if (tmpX == 0 || tmpY == 0) break;

                circle(image, Point(tmpX, tmpY), 1, green, 1);
                //std::cout << tmpX << ", " << tmpY << std::endl;
            } while (true);
        }

        std::cout << "Time: " << (MPI_Wtime() - start) * 1000 << " ms" << std::endl;
        std::cout << "Working processes amount: " << mpiSize - 1 << std::endl;
        imshow("Output", image);
        waitKey(0);
        system("pause");
    }
#pragma endregion

#pragma region sender
    else
    {
        Point vertexPoints[numOfPoints]{
            {10, 100},
            {100, 100},
            {200, 200},
            {400, 10},
            {500, 300},
            {200, 500},
            {180, 400},
            {10, 350}
        };

        int xMin = vertexPoints[0].x, xMax = vertexPoints[0].x, yMin = vertexPoints[0].y, yMax = vertexPoints[0].y;
        for (int i = 1; i < numOfPoints; ++i)
        {
            if (vertexPoints[i].x < xMin) xMin = vertexPoints[i].x;
            else if (vertexPoints[i].x > xMax) xMax = vertexPoints[i].x;
            if (vertexPoints[i].y < yMin) yMin = vertexPoints[i].y;
            else if (vertexPoints[i].y > yMax) yMax = vertexPoints[i].y;
        }

        const int yLengthPerProc = std::ceil((yMax - yMin) / (mpiSize - 1));
        yMin = yMin + (mpiRank - 1) * yLengthPerProc;
        yMax = yMin + yLengthPerProc;

        std::cout << "[#" << mpiRank << "] " << "Sender starts from " << yMin << " to " << yMax << std::endl;
        drawFun(imgPartCoords, vertexPoints, numOfPoints, xMin, xMax, yMin, yMax, mpiRank);
        MPI_Send(imgPartCoords, maxBufferSize, MPI_INT, 0, 0, MPI_COMM_WORLD);
    }
#pragma endregion

    delete[] imgPartCoords;
    MPI_Finalize();
    return 0;
}